/*Louisa Yuxin Lin 1933472*/
package Geometry;
public class LotsOfShapes{
  public static void main(String[] args){
    Shape[] shapes =new Shape[5];
    shapes[0]= new Rectangle(5,7);
    shapes[1]= new Circle(5);
    shapes[2]= new Rectangle(9,4);
    shapes[3]= new Circle(8);
    shapes[4]= new Square(4);
      
      
  
  for (Shape s:shapes){
    System.out.println("The area is: "+s.getArea()+", perimeter is: "+s.getPerimeter());
  }
}
}