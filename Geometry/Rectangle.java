/*Louisa Yuxin Lin 1933472*/
package Geometry;
public class Rectangle implements Shape{
 private double length;
 private double width;
 
 public Rectangle(double l, double w) {
  this.length=l;
  this.width=w;
 }
 
 public double getLength() {
  return this.length;
 }
 
 public double getWidth() {
  return this.width;
 }
 
 public double getArea() {
  double area = this.length * this.width;
  return area;
 }
 
 public double getPerimeter() {
  double perimeter=2 * (this.length + this.width);
  return perimeter;
 
 
}

}