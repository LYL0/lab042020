/*Louisa Yuxin Lin 1933472*/
package Geometry;
public interface Shape{
  double getArea();
  double getPerimeter();
}
