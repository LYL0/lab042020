/*Louisa yuxin Lin 1933472*/
package Inheritance;
public class Book{
  protected String title;
  private String author;
  
  public Book(String t, String a){
    this.title =t;
    this.author =a;
  }
  public String getTitle(){
    return this.title;
  }
  public String getAuthor(){
    return this.author;
  }
  public String toString(){
    return ""+this.title+","+this.author;
  }
}