/*Louisa Yuxin Lin 1933472*/
package Inheritance;
public class BookStore{
  public static void main(String[]args){
    Book[] books =new Book[5];
    books[0]=new Book("Pomegranate Soup","Marsha Mehran");
    books[1]=new ElectronicBook("Antigone","Sophocles",890);
    books[2]=new Book("The Outsider","Albert Camus");
    books[3]=new ElectronicBook("The Last Chinese Chief","Nicole Mones",900);
    books[4]=new ElectronicBook("The Metamorphosis","Franz Kafka",346);
  
  for(Book b:books){
    System.out.println(b);
  }
  }
}
