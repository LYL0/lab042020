/*Louisa Yuxin Lin 1933472*/
package Inheritance;
public class ElectronicBook extends Book{
  private int numberBytes;
  public ElectronicBook(String title,String author,int b){
    super(title,author);
    this.numberBytes=b;
  }
  public int getNumberBytes(){
    return this.numberBytes;
  }
  public String toString(){
    String fromBaseClass =super.toString();
    return fromBaseClass+","+this.numberBytes;
  }
}